package org.example.kafkabeginnerscourse;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

import static org.apache.kafka.clients.producer.ProducerConfig.*;

public class ProducerDemoKeys {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        var properties = new Properties();
        properties.setProperty(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.setProperty(KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        var producer = new KafkaProducer<String, String>(properties);
        for (int i = 0; i < 10; i++) {

            var record = new ProducerRecord<>("first_topic", "key" + (i%5), "Guten Abend " + i);
            producer.send(record, (recordMetadata, e) -> {
                if (e == null) {
                    System.out.println("Received new metadata" +
                            "\nTopic: " + recordMetadata.topic() +
                            "\nPartition: " + recordMetadata.partition() +
                            "\nOffset: " + recordMetadata.offset() +
                            "\nTimestamp: " + recordMetadata.timestamp());
                } else {
                    System.err.println("Error while producing: " + e);
                }
            }).get(); // <- force Synchronization, don't do this is production!;
            producer.flush();
        }
        producer.close();
    }
}
