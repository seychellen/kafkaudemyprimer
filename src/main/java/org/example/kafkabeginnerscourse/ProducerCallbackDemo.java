package org.example.kafkabeginnerscourse;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

import static org.apache.kafka.clients.producer.ProducerConfig.*;

public class ProducerCallbackDemo {

    public static void main(String[] args) {

        var properties = new Properties();
        properties.setProperty(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.setProperty(KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        var producer = new KafkaProducer<String, String>(properties);
        var record = new ProducerRecord<String, String>("first_topic", "Guten Morgen Bruno");
        producer.send(record, (recordMetadata, e) -> {
            if (e == null) {
                System.out.println("Received new metadata" +
                        "\nTopic: " + recordMetadata.topic() +
                        "\nPartition: " + recordMetadata.partition() +
                        "\nOffset: " + recordMetadata.offset() +
                        "\nTimestamp: " + recordMetadata.timestamp());
            } else {
                System.err.println("Error while producing: " + e);
            }
        });
        producer.close();
    }
}
